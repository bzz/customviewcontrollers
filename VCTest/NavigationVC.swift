import UIKit



class NavigationVC: MBTabParentViewController {
   convenience init() {
      self.init(buttonTag:3,
                backgroundImageName:"",
                backgroundColor:UIColor(hex: 0xffffff))
      let vc1 = FirstVC()
      let vc2 = SecondVC()
      self.viewControllers = (nil, vc1, nil, vc2, nil)
      self.buttonImageNames = (nil, "icon_news", nil, "icon_messages", nil)
   }
   
   
   override func viewDidLoad() {
      super.viewDidLoad()
   }
   
   
}

