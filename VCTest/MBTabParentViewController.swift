import UIKit


class MBTabParentViewController: UIViewController {

   var currentButtonTag = 0
   var backgroundImageName = "background"
   var bottomBarColor = UIColor.white
   var viewControllers : (UIViewController?,UIViewController?,UIViewController?,UIViewController?,UIViewController?)
      = (nil, nil, nil, nil, nil)
   var buttonImageNames : (String?, String?, String?, String?, String?)
      = (nil, nil, nil, nil, nil)


   
   convenience init() {
      self.init(buttonTag:1, backgroundImageName:"background", backgroundColor:UIColor.white)
   }
   init(buttonTag:Int, backgroundImageName:String, backgroundColor:UIColor) {
      super.init(nibName: nil, bundle: nil)
      currentButtonTag = buttonTag
      self.backgroundImageName = backgroundImageName
      self.bottomBarColor = backgroundColor
   }
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
   
   
   let statusBarHeight = UIApplication.shared.statusBarFrame.height
   let screenHeight = UIScreen.main.bounds.height
   let screenWidth = UIScreen.main.bounds.width
   let btnHeight :CGFloat = 49 * UIScreen.main.bounds.width / 320
   let btnWidth  :CGFloat = 64 * UIScreen.main.bounds.width / 320


   var bottomBar:UIView!

   var mainViewBed = UIView()
   var currentVC:UIViewController!
   var startVC : UIViewController?
   
   
   override func viewDidLoad() {
      super.viewDidLoad()
      self.startVC = vcForButtonTag(currentButtonTag)

      let background = UIImageView(frame:view.bounds)
      background.image = UIImage(named:backgroundImageName);
      background.backgroundColor = bottomBarColor
      view.addSubview(background)

      mainViewBed = UIView(frame: CGRect(x: 0, y: statusBarHeight, width: screenWidth, height: screenHeight-statusBarHeight-btnHeight))
      view.addSubview(mainViewBed)
      
      bottomBar = UIView(frame: CGRect(x: 0, y: screenHeight-btnHeight, width: screenWidth, height: btnHeight))
      bottomBar.backgroundColor = bottomBarColor
      view.addSubview(bottomBar)

      
      addPanelButton(0, image:buttonImageNames.0)
      addPanelButton(1, image:buttonImageNames.1)
      addPanelButton(2, image:buttonImageNames.2)
      addPanelButton(3, image:buttonImageNames.3)
      addPanelButton(4, image:buttonImageNames.4)

      if (startVC != nil) {
         let toVC = startVC!
         self.addChild(toVC)
         self.mainViewBed.addSubview(toVC.view)
         toVC.didMove(toParent: self)
         self.currentVC = toVC
      }
      

   }

   
   
   func vcForButtonTag(_ buttonTag : Int) -> UIViewController {
      var toVC = UIViewController()
      switch buttonTag {
         case 0:
            toVC = self.viewControllers.0 ?? UIViewController()
         case 1:
            toVC = self.viewControllers.1 ?? UIViewController()
         case 2:
            toVC = self.viewControllers.2 ?? UIViewController()
         case 3:
            toVC = self.viewControllers.3 ?? UIViewController()
         case 4:
            toVC = self.viewControllers.4 ?? UIViewController()
         default: break
      }
      
      return toVC
   }

   
   
   
   
   
   
   func addPanelButton(_ tag:NSInteger, image:String?) {
      let button = UIButton(frame: CGRect(x: btnWidth*CGFloat(tag), y: 0, width: btnWidth, height: btnHeight))
      guard image != nil else { return }
      button.setBackgroundImage(UIImage(named: image!), for: UIControl.State())
      button.setBackgroundImage(UIImage(named: image! + "_active"), for: .selected)
      button.adjustsImageWhenHighlighted = false
      button.addTarget(self, action:#selector(self.panelButtonPressed(_:)), for: UIControl.Event.touchDown)
      button.tag = tag
      if tag == currentButtonTag {
         button.isSelected = true
      }
      bottomBar.addSubview(button)
   }
   
   
   @objc func panelButtonPressed(_ button:UIButton) {
      for v:UIView in bottomBar.subviews {
         if v.isKind(of: UIButton.self) {
            let b = v as! UIButton
            b.isSelected = false
         }
      }
      button.isSelected = true;
      let toVC = vcForButtonTag(button.tag)
      if currentVC.classForCoder == toVC.classForCoder {return}
      self.currentVC!.willMove(toParent: nil)
      self.currentVC!.removeFromParent()
      self.addChild(toVC)
      self.mainViewBed.addSubview(toVC.view)
      let bedCenter = mainViewBed.convert(mainViewBed.center, from: view)
      let completion = { (value: Bool) in
         self.currentVC!.view.removeFromSuperview()
         self.currentVC = toVC
         self.bottomBar.isUserInteractionEnabled = true
      }
      self.bottomBar.isUserInteractionEnabled = false
      toVC.view.center.y = bedCenter.y
      if self.currentButtonTag < button.tag {
         toVC.view.center.x = screenWidth + bedCenter.x
         UIView.animate(withDuration: 0.1, animations: {
            self.currentVC?.view.center.x = -self.screenWidth + (self.currentVC?.view.center.x)!
            toVC.view.center = bedCenter
         }, completion: completion)
      } else {
         toVC.view.center.x = -bedCenter.x
         UIView.animate(withDuration: 0.1, animations: {
            self.currentVC?.view.center.x = self.screenWidth + (self.currentVC?.view.center.x)!
            toVC.view.center = bedCenter
         }, completion: completion)
      }
      toVC.didMove(toParent: self)
      self.currentButtonTag = button.tag
   }
   
}

