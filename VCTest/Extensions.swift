import UIKit


extension UIColor {
   public convenience init(hex: UInt32, alpha: CGFloat = 1) {
      let divisor = CGFloat(255)
      let red     = CGFloat((hex & 0xFF0000) >> 16) / divisor
      let green   = CGFloat((hex & 0x00FF00) >>  8) / divisor
      let blue    = CGFloat( hex & 0x0000FF       ) / divisor
      self.init(red: red, green: green, blue: blue, alpha: alpha)
   }
   public convenience init(hexA: UInt32) {
      let divisor = CGFloat(255)
      let red     = CGFloat((hexA & 0xFF000000) >> 24) / divisor
      let green   = CGFloat((hexA & 0x00FF0000) >> 16) / divisor
      let blue    = CGFloat((hexA & 0x0000FF00) >>  8) / divisor
      let alpha   = CGFloat( hexA & 0x000000FF       ) / divisor
      self.init(red: red, green: green, blue: blue, alpha: alpha)
   }
}

