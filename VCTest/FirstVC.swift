import UIKit



class FirstVC: MBTabChildViewController {

   var bottomBar = UIView()
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      
      bottomBar = UIView(frame: CGRect(x: 0, y: 0, width: parentVC.screenWidth, height: parentVC.btnHeight))
      bottomBar.backgroundColor = UIColor(hex: 0xff1111)
      view.addSubview(bottomBar)

      self.view.backgroundColor = UIColor(hex: 0xffeeee)
  }


}

